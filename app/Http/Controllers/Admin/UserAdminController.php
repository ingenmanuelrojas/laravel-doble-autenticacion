<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\UsuarioAdminRequest;
use App\Models\Admin;
use Illuminate\Http\Request;
use Log;
use PhpParser\Node\Stmt\TryCatch;

class UserAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin'); 
    }
 
    public function index()
    {
        return view('admin.users');
    }

    public function listado(){
        try {
            return response()->json(Admin::orderBy('id','desc')->get(), 200);
        } catch (\Exception $e) {
            $this->controlarExcepcion($e, 'UserAdminController', 'listado');
            return $this->response(500, 'Error del servidor, intentelo de nuevo.', 'error');
        }
    }

    public function buscar(Request $request){
        try {
            return response()->json(Admin::where('id',$request->user_id)->first(), 200);
        } catch (\Exception $e) {
            $this->controlarExcepcion($e, 'UserAdminController', 'buscar');
            return $this->response(500, 'Error del servidor, intentelo de nuevo.', 'error');
        }
    }

    public function guardar(UsuarioAdminRequest $request){
        try {
            $user = Admin::create([
                'name'     => $request->nombre,
                'lastName' => $request->apellido,
                'email'    => $request->email,
                'phone'    => $request->telefono,
                'password' => bcrypt($request->password),
            ]);

            return response()->json([
                "code" => 200,
                "message" => 'Usuario creado con exito',
            ], 200);
        } catch (\Exception $e) {
            $this->controlarExcepcion($e, 'UserAdminController', 'guardar');
            return $this->response(500, 'Error del servidor, intentelo de nuevo.', 'error');
        }
    }
    
    public function editar(UsuarioAdminRequest $request){
        try {
            $user = Admin::where('id', $request->id)->first();
            $user->name = $request->nombre;
            $user->lastName = $request->apellido;
            $user->email = $request->email;
            $user->phone = $request->telefono;
            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json([
                "code" => 200,
                "message" => 'Usuario modificado con exito',
            ], 200);
        } catch (\Exception $e) {
            $this->controlarExcepcion($e, 'UserAdminController', 'editar');
            return $this->response(500, 'Error del servidor, intentelo de nuevo.', 'error');
        }
    }

    public function eliminar($user_id)
    {
        try {
            $user = Admin::find($user_id);
            $user->delete();

            return response()->json([
                'status'  => 'success',
                'code'    => 200,
                'message' => 'Usuario eliminado correctamente.'
            ], 200);
        } catch (\Exception $e) {
            $this->controlarExcepcion($e, 'UserAdminController', 'eliminar');
            return $this->response(500, 'Error del servidor, intentelo de nuevo.', 'error');
        }
    }
}
