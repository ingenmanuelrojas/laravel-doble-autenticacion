<?php

namespace App\Http\Requests\admin;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Log;
class UsuarioAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules(){
        Log::info($this->method());
        switch ($this->method()) {
            case 'POST':   
                $rules = [
                    'nombre'   => 'required|regex:/^[\pL\s\-]+$/u',
                    'apellido' => 'required|regex:/^[\pL\s\-]+$/u',
                    'telefono' => 'required|digits:9|',
                    'email'   => 'required|email|unique:admins,email',
                    'password' => 'required|min:8',
                ];
            break;
            case 'PUT':
                $rules = [
                    'nombre'   => 'required|regex:/^[\pL\s\-]+$/u',
                    'apellido' => 'required|regex:/^[\pL\s\-]+$/u',
                    'telefono' => 'required|digits:9|',
                    'email'     => ['required','email', Rule::unique('admins')->ignore($this->id)],
                    // 'password'  => 'required|min:8',
                ];
            break;
        }
        return $rules;
    }

    public function messages(){
        return [
            'nombre.required'   => 'El nombre es obligatorio',
            'apellido.required' => 'El apellido es obligatorio',
            'nombre.regex'      => 'El nombre acepta solo letras',
            'apellido.regex'    => 'El apellido acepta solo letras',
            'telefono.required' => 'El teléfono es obligatorio',
            'telefono.digits'   => 'El teléfono deben ser 9 digitos',
            'email.required'    => 'El email es obligatorio',
            'email.unique'      => 'El email ya se encuentra registrado',
            'password.required' => 'La clave es obligatoria',
            'password.min'      => 'La clave debe ser de al menos 8 caracteres',
        ];
     }
}
