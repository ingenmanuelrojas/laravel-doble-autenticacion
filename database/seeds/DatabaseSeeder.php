<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesAndPermissionsSeeder::class);

        DB::table('users')->insert(
        	[
	            'name' => 'Ana Web',
	            'email' => 'ana@gmail.com',
	            'password' => Hash::make('password'),
        	]
        );


        DB::table('admins')->insert(
        	[
	            'name' => 'Luis ',
	            'lastName' => 'Admin',
	            'phone' => '123456789',
	            'email' => 'luis@gmail.com',
	            'password' => Hash::make('password'),
        	]
        );

        DB::table('admins')->insert(
        	[
	            'name' => 'Juan',
				'lastName' => 'Admin',
	            'phone' => '123456789',
	            'email' => 'juan@gmail.com',
	            'password' => Hash::make('password'),
        	]
        );

        DB::table('model_has_roles')->insert(
        	[
	            'role_id'    => 1,
	            'model_type' => 'App\Models\User',
	            'model_id'   => 1,
        	]
        );

        DB::table('model_has_roles')->insert(
        	[
	            'role_id'    => 2,
	            'model_type' => 'App\Models\Admin',
	            'model_id'   => 1,
        	]
        );

        DB::table('model_has_roles')->insert(
        	[
	            'role_id'    => 3,
	            'model_type' => 'App\Models\Admin',
	            'model_id'   => 2,
        	]
        );
    }
}
