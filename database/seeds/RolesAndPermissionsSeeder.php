<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['guard_name' => 'web','name' => 'edit articles']);
        Permission::create(['guard_name' => 'admin','name' => 'edit articles']);
        Permission::create(['guard_name' => 'admin','name' => 'delete articles']);
        Permission::create(['guard_name' => 'admin','name' => 'publish articles']);
        Permission::create(['guard_name' => 'admin','name' => 'unpublish articles']);


        // this can be done as separate statements
        $role = Role::create(['guard_name' => 'web','name' => 'writer']);
        $role->givePermissionTo('edit articles');

        // or may be done by chaining
        $role = Role::create(['guard_name' => 'admin','name' => 'moderator'])
            ->givePermissionTo(['publish articles', 'unpublish articles']);

        $role = Role::create(['guard_name' => 'admin','name' => 'super-admin']);
        $role->givePermissionTo(['edit articles', 'delete articles', 'publish articles', 'unpublish articles']);
    }
}
