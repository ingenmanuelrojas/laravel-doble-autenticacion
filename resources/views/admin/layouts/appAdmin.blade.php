<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Administrador</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="{{ asset('assets/admin/plugins/fontawesome-free/css/all.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('assets/admin/css/adminlte.css') }}">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            @include('admin.layouts.header')
            
            @include('admin.layouts.menu')            

            <div class="content-wrapper" id="app">
                @yield('content')
            </div>

            @include('admin.layouts.footer') 
        </div>

        <script src="{{ asset('js/admin.js') }}"></script>
        <script src="{{asset('assets/admin/plugins/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/adminlte.min.js')}}"></script>
    </body>
</html>
