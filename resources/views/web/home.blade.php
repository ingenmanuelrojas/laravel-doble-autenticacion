@extends('web.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    You are logged in! <b>Web</b>
                    <hr>

                    @can('edit articles')
                        <h3>Puedes Editar articulos</h3>
                        <hr>
                    @endcan

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
