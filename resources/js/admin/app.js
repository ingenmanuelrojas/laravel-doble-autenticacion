
require('./bootstrap');
import Swal from 'sweetalert2';
import Vue from 'vue';
import Vuesax from 'vuesax';
import 'vuesax/dist/vuesax.css';
import VuePaginate from 'vue-paginate';

Vue.use(Vuesax, {
    // options here
  })

window.Vue = require('vue');
window.Swal = Swal;
Vue.use(VuePaginate)


Vue.component('dashboard-component', require('./components/Dashboard.vue').default);
Vue.component('users-admin-component', require('./components/UsersAdmin.vue').default);

const app = new Vue({
    el: '#app',
});

