<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('web.welcome');
});

Auth::routes();

Route::get('/home', 'Web\HomeController@index')->name('home');
Route::get('/user/logout', 'Auth\LoginController@logoutUser')->name('user.logout');


Route::prefix('admin')->group(function(){
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/', 'Admin\AdminController@index')->name('admin.dashboard');
    Route::get('/usuarios-administrador', 'Admin\UserAdminController@index')->name('admin.user.admins');
    Route::post('/usuarios-administrador/guardar', 'Admin\UserAdminController@guardar')->name('admin.user.guardar');
    Route::get('/usuarios-administrador/listado', 'Admin\UserAdminController@listado')->name('admin.user.listado');
    Route::post('/usuarios-administrador/buscar', 'Admin\UserAdminController@buscar')->name('admin.user.buscar');
    Route::put('/usuarios-administrador/editar', 'Admin\UserAdminController@editar')->name('admin.user.editar');
    Route::delete('/usuarios-administrador/eliminar/{user_id}', 'Admin\UserAdminController@eliminar')->name('admin.user.eliminar');

});