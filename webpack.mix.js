const mix = require('laravel-mix');


   mix.js('resources/js/admin/app.js', 'public/js/admin.js')
   .sass('resources/sass/app.scss', 'public/css/admin_sass.css')
   .js(['resources/js/web/app.js'], 'public/js/web.js')
//    .sass('resources/sass/web.scss', 'public/css/web_sass.css')
   .version();