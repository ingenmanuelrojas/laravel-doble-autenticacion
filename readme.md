**LARAVEL, DOBLE AUTENTICACIÓN CON GUARDS, PERMISOS Y ROLES CON SPATIE**


*Instalación:*

*  Clonar el repositorio.
*  Crear base de datos.
*  Ejecutar composer install
*  Correr las Migraciones
*  Ejecutar los Seeders

*Usuario Web:* http://localhost:8000

    Email: ana@gmail.com    
    password: password
    Rol: writer

*Usuarios Admin:* http://localhost:8000/admin

    Email: luis@gmail.com    
    password: password
    Rol: moderator

    Email: juan@gmail.com    
    password: password
    Rol: super-admin
